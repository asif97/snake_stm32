#include "snake.h"

#define TEST_CNT 	16
#define INIT_LENGTH 3
#define	HEAD 		0
#define SQR_PIX		4	/* each point will be a square of SQR_PIX X SQR_PIX  */

#define UP_THRESHOLD		3500
#define LOW_THRESHOLD		500

extern ADC_HandleTypeDef hadc1;

point_t snake[MAX_LENGTH];				/* Snake array */
int c_length = INIT_LENGTH;
food_t food;							/* Food */

error_t
play_snake (void)
{
	static point_t c_temp, o_temp;
	static char c_dir;
	static comm_t command = NONE;
	static uint8_t i;

	c_temp = snake[HEAD];
	c_dir = snake[HEAD].dir;

	for (i = 0; i < 10; ++i)
	{
		static comm_t old_comm = NONE;
		old_comm = command;
		command = get_dir(c_dir);
		if (command != old_comm)
		{
			while (i < 10)
			{
				HAL_Delay(10);
				++i;
			}
		}
		HAL_Delay(10);
	}

	switch (c_dir)
	{
		case RIGHT:
			if(command == NONE) {
				snake[HEAD].x += 1;
			} else if(command == TURN_RIGHT) {
				snake[HEAD].dir = DOWN;
				snake[HEAD].y += 1;
			} else { //TURN LEFT
				snake[HEAD].dir = UP;
				snake[HEAD].y -= 1;
			}
			break;
		case UP:
			if(command == NONE) {
				snake[HEAD].y -= 1;
			} else if(command == TURN_RIGHT) {
				snake[HEAD].dir = RIGHT;
				snake[HEAD].x += 1;
			} else {
				snake[HEAD].dir = LEFT;
				snake[HEAD].x -= 1;
			}
			break;
		case LEFT:
			if(command == NONE) {
				snake[HEAD].x -= 1;
			} else if(command == TURN_RIGHT) {
				snake[HEAD].dir = UP;
				snake[HEAD].y -= 1;
			} else {
				snake[HEAD].dir = DOWN;
				snake[HEAD].y += 1;
			}
			break;
		case DOWN:
			if(command == NONE) {
				snake[HEAD].y += 1;
			} else if(command == TURN_RIGHT) {
				snake[HEAD].dir = LEFT;
				snake[HEAD].x -= 1;
			} else {
				snake[HEAD].dir = RIGHT;
				snake[HEAD].x += 1;
			}
			break;
		default:
			break;
	}
	if(snake[HEAD].x == (MIN_X - 1)
	   || snake[HEAD].x == (MAX_X + 1)
	   || snake[HEAD].y == (MIN_Y - 1)
	   || snake[HEAD].y == (MAX_Y + 1))
	{
		//return 0;
	}
	/*After updating the head, the i+1 element take the i element's values*/
	for(int j = 1; j < c_length; j++){
		o_temp = snake[j];
		snake[j] = c_temp;
		c_temp = o_temp;
	}

	/*check if head == food, then add a point after the tail*/
	if(snake[HEAD].x == food.x && snake[0].y == food.y) {
		switch (snake[c_length - 1].dir)
		{
			case RIGHT:
				snake[c_length].dir = RIGHT;
				snake[c_length].x = snake[c_length - 1].x - 1;
				snake[c_length].y = snake[c_length - 1].y;
				break;
			case UP:
				snake[c_length].dir = UP;
				snake[c_length].y = snake[c_length - 1].y + 1;
				snake[c_length].x = snake[c_length - 1].x;
				break;
			case LEFT:
				snake[c_length].dir = LEFT;
				snake[c_length].x = snake[c_length - 1].x + 1;
				snake[c_length].y = snake[c_length - 1].y;
				break;
			case DOWN:
				snake[c_length].dir = DOWN;
				snake[c_length].y = snake[c_length - 1].y - 1;
				snake[c_length].x = snake[c_length - 1].x;
				break;
			default:
				break;
		}
		c_length += 1;
		food = random_food_pos();
		draw_food(food);
	}

	print_snake(snake, c_length, snake[0], c_temp);

	return ALL_OK;
}

error_t
print_snake (point_t snake[], int lenght, point_t head, point_t old_tail)
{
    if(lenght > MAX_LENGTH){
        return ERROR;
    }
    //SSD1306_Clear();
    uint16_t x, y, pixels;
    pixels = SQR_PIX;
    x = old_tail.x * SQR_PIX;
    y = old_tail.y * SQR_PIX;
    SSD1306_DrawFilledRectangle(x, y, pixels, pixels, SSD1306_COLOR_BLACK);
    x = head.x * SQR_PIX;
    y = head.y * SQR_PIX;
    SSD1306_DrawFilledRectangle(x, y, pixels, pixels, SSD1306_COLOR_WHITE);
    SSD1306_UpdateScreen();
//    for (int i = 0; i < lenght; i++)
//    {
//    	x = snake[i].x * SQR_PIX;
//    	y = snake[i].y * SQR_PIX;
//		SSD1306_DrawFilledRectangle(x, y, pixels, pixels, SSD1306_COLOR_WHITE);
//		SSD1306_UpdateScreen();
//    }

    return ALL_OK;
}

error_t
init_snake (void)
{
	SSD1306_Clear();
	/* Draw initial snake */
	food = random_food_pos();		/* Create food and initialize at random position */
	draw_food(food);

	if(c_length > MAX_LENGTH){
		return ERROR_1;
	}

	/* Initialize snake array */
	int temp = c_length - 1;
	for(int i = 0; i < c_length; i++) {
		snake[i].dir = RIGHT;
		snake[i].x = temp;
		snake[i].y = 10;
		temp--;
	}

	point_t head = snake[0];
	point_t tail = snake[c_length - 1];

	uint16_t x, y, pixels;
	pixels = SQR_PIX;
	x = tail.x * pixels;
	y = tail.y * pixels;
	for(uint8_t i = 0; i <= head.x; i++) {
		SSD1306_DrawFilledRectangle(x, y, pixels, pixels, SSD1306_COLOR_WHITE);
		SSD1306_UpdateScreen();
		x += SQR_PIX;
	}
	HAL_Delay(2000);
	return ALL_OK;
}

food_t
random_food_pos (void)
{
	static uint8_t x = 11, m_x = 31, a_x = 2, c_x = 1;
	static uint8_t y = 7, m_y = 17, a_y = 1, c_y = 1;
	food_t new_food;
	x = (a_x * x + c_x) % m_x;
	y = (a_y * y + c_y) % m_y;
	if (x > (128/4)) {
		x -= (128/4);
	}
	if (y > (64/4)) {
		y += (64/4);
	}
	new_food.x = x;
	new_food.y = y;

	return new_food;
}

error_t
draw_food (food_t food) {
	uint16_t x, y, pixels;
	pixels = SQR_PIX;
	x = food.x * pixels;
	y = food.y * pixels;
	SSD1306_DrawFilledRectangle(x, y, pixels, pixels, SSD1306_COLOR_WHITE);
	SSD1306_UpdateScreen();
	return ALL_OK;
}

static uint32_t adc_val_x, adc_val_y;
HAL_StatusTypeDef a;
comm_t
get_dir (dir_t curr_dir)
{
	HAL_ADC_Start(&hadc1);
	a = HAL_ADC_PollForConversion(&hadc1, 100);
	adc_val_y = HAL_ADC_GetValue(&hadc1);
	HAL_ADC_PollForConversion(&hadc1, 100);
	adc_val_x = HAL_ADC_GetValue(&hadc1);
	HAL_ADC_Stop(&hadc1);

	adc_val_y = 4096 - adc_val_y;

	switch (curr_dir) {
		case RIGHT:
			if (adc_val_y < LOW_THRESHOLD)
			{
				return TURN_RIGHT;
			} else if (adc_val_y > UP_THRESHOLD)
			{
				return TURN_LEFT;
			} else
			{
				return NONE;
			}
			break;

		case LEFT:
			if (adc_val_y < LOW_THRESHOLD)
			{
				return TURN_LEFT;
			} else if (adc_val_y > UP_THRESHOLD)
			{
				return TURN_RIGHT;
			} else
			{
				return NONE;
			}
			break;

		case DOWN:
			if (adc_val_x < LOW_THRESHOLD)
			{
				return TURN_RIGHT;
			} else if (adc_val_x > UP_THRESHOLD)
			{
				return TURN_LEFT;
			} else
			{
				return NONE;
			}
			break;

		case UP:
			if (adc_val_x < LOW_THRESHOLD)
			{
				return TURN_LEFT;
			} else if (adc_val_x > UP_THRESHOLD)
			{
				return TURN_RIGHT;
			} else
			{
				return NONE;
			}
			break;

		default:
			break;
	}

	return NONE;
}
