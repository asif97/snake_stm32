#include "main.h"
#include "ssd1306.h"

#define MAX_LENGTH 50
#define MIN_X 0
#define MIN_Y 0
#define MAX_X 128
#define MAX_Y 64

typedef enum {
	ALL_OK,
    LENGTH_ERR,
    ERROR_1
} error_t;

typedef enum {
    NONE, TURN_RIGHT, TURN_LEFT
} comm_t;

typedef enum {
	LEFT, RIGHT, UP, DOWN
} dir_t;

typedef struct {
    int x;
    int y;
    dir_t dir;
} point_t;

typedef struct
{
    int x;
    int y;
} food_t;


error_t print_snake (point_t snake[], int length, point_t head, point_t old_tail);
error_t init_snake (void);
error_t draw_food(food_t food);
comm_t get_dir (dir_t curr_dir);
food_t random_food_pos(void);
error_t play_snake (void);
